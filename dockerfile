FROM golang:1.14

WORKDIR /go/src/app
RUN echo "devops"
COPY . .

RUN go get -d -v ./...
RUN go install -v ./...
EXPOSE 5000

CMD ["app"]
